;(function() {
  'use strict';
  angular
    .module('boilerplate')
    .factory('$in', [
      '$injector', InjectorService
    ]);
    function InjectorService($injector) {
        return {
          remote : function(){
            return {
              BlockBuilder: $injector.get('BlockBuilderRemoteService')
            }
          },
          local: function(){
            return {
              BlockBuilder: $injector.get('BlockBuilderLocalService'),
              User: $injector.get('UserLocalService')
            }
          },
          model: function(){
            return {
              BlockBuilder: $injector.get('BlockBuilderModel'),
              SignIn: $injector.get('SignInModel')
            }
          },
          storage: {},
          utils: {},
          const: {}
        };
    }
})();
