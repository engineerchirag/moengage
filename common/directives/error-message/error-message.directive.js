;(function() {

  'use strict';
  angular
    .module('boilerplate')
    .directive('errorMessage', errorMessage);

  function errorMessage() {
    var directiveDefinitionObject = {
      restrict: 'AE',
      templateUrl: 'common/directives/error-message/error-message.html',
      scope: {
        formName: "=",
        elementName: "="
      }
    };

    return directiveDefinitionObject;
  }

})();
