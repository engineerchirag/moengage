;(function() {
  'use strict';
  angular
    .module('boilerplate')
    .factory('SignInModel', [
      '$window', '$rootScope', SignInModel
    ]);
    function SignInModel($window, $rootScope) {

      function SignIn(id, firstName, lastName, emailId, password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailId = emailId;
        this.password = password;
      }
      SignIn.build = function (data) {
        data = data ? data : {};
        return new SignIn(
          data.id,
          data.firstName,
          data.lastName,
          data.emailId,
          data.password
        );
      };
      SignIn.buildCollection  = function(collection) {
        return angular.isArray(collection) ? collection.map(SignIn.build) : [];
      };

      SignIn.apiResponseTransformer = function (responseData) {
        if (angular.isArray(responseData)) {
          return responseData.map(SignIn.build);
        }
        return SignIn.build(responseData);
      };
      return SignIn;
    }

})();
