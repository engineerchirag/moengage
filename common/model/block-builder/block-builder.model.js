;(function() {
  'use strict';
  angular
    .module('boilerplate')
    .factory('BlockBuilderModel', [
      '$window', '$rootScope', BlockBuilderModel
    ]);
    function BlockBuilderModel($window, $rootScope) {

      function BlockBuilder(id) {
        this.id = id;
      }
      BlockBuilder.build = function (data) {
        return new BlockBuilder(
          data.id
        );
      };
      BlockBuilder.buildCollection  = function(collection) {
        return angular.isArray(collection) ? collection.map(BlockBuilder.build) : [];
      };

      BlockBuilder.apiResponseTransformer = function (responseData) {
        if (angular.isArray(responseData)) {
          return responseData.map(BlockBuilder.build);
        }
        return BlockBuilder.build(responseData);
      };
      return BlockBuilder;
    }

})();
