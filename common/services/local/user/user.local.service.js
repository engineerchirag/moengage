;(function() {
  'use strict';
  angular
    .module('boilerplate')
    .factory('UserLocalService', [
      '$in', '$q', UserLocalService
    ]);
  function UserLocalService($in, $q) {
    return {
      logger: function logger(data){
        return console.log(data);
      },
      validateUser: function (user){
        var msg = {
          'message': ''
        };
        var deferred = $q.defer();
        if(user.emailId === 'demo@moengage.com' && user.password === 'password'){
          msg.message = 'Login Successful';
          deferred.resolve(msg);
        }else{
          msg.message = "Username and password doesn't match";
          deferred.reject(msg);
        }
        return deferred.promise;
      }
    };
  }
})();
