;(function() {
  'use strict';
  angular
    .module('boilerplate')
    .factory('BlockBuilderLocalService', [
      '$in', BlockBuilderService
    ]);
  function BlockBuilderService($in) {
    return {
      logger: function logger(data){
        return console.log(data);
      },
      getData: function (){
        return $in.remote().BlockBuilder.getData();
      }
    };
  }
})();
