;(function() {

  angular
    .module('boilerplate')
    .controller('SignInController', SignInController);

  SignInController.$inject = ['$scope', '$in', 'ngNotify', '$state', '$rootScope'];
  function SignInController($scope, $in, ngNotify, $state, $rootScope) {
    $rootScope.headerVisibility = false;
    $scope.user = $in.model().SignIn.build();
    $scope.signInVisibility = true;

    $scope.showSignInForm = function(visibility){
      $scope.signInVisibility = visibility;
    };

    $scope.userSignUp = function () {
      console.log($scope.user);
      $scope.signInVisibility = false;
      $scope.user.password = null;
    };

    $scope.userSignIn = function(){
      $in.local().User.validateUser($scope.user).then(function(data){
        ngNotify.set(data.message);
        $state.go('blockBuilder');
      },function(err){
        ngNotify.set(err.message, {
          type: 'error',
          duration: 2000
        });
      });
    };
  }


})();
