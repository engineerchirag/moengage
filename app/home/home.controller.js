;(function() {

  angular
    .module('boilerplate')
    .controller('HomeController', HomeController);

  HomeController.$inject = ['$rootScope'];
  function HomeController($rootScope) {
    $rootScope.headerVisibility = true;
  }


})();
