/**
 *
 * AngularJS Boilerplate
 * @description           Description
 * @author                Jozef Butko // www.jozefbutko.com/resume
 * @url                   www.jozefbutko.com
 * @version               1.1.7
 * @date                  March 2015
 * @license               MIT
 *
 */
;(function() {


  /**
   * Definition of the main app module and its dependencies
   */
  angular
    .module('boilerplate', [
      'ui.router',
      'ngLodash',
      'ngMessages',
      'ngNotify'
    ])
    .config(config);

  // safe dependency injection
  // this prevents minification issues
  config.$inject = ['$urlRouterProvider', '$stateProvider', '$locationProvider', '$httpProvider', '$compileProvider'];

  /**
   * App routing
   *
   * You can leave it here in the config section or take it out
   * into separate file
   *
   */
  function config($urlRouterProvider, $stateProvider, $locationProvider, $httpProvider, $compileProvider) {

    $locationProvider.html5Mode(false);

    $urlRouterProvider.otherwise('/sign-in');

    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/home/home.view.tpl.html',
        controller: 'HomeController'
      })
      .state('blockBuilder', {
        url: '/block-builder',
        templateUrl: 'app/block-builder/block-builder.view.tpl.html',
        controller: 'BlockBuilderController'
      })
      .state('signIn', {
        url: '/sign-in',
        templateUrl: 'app/sign-in/sign-in.view.tpl.html',
        controller: 'SignInController'
      });

    $httpProvider.interceptors.push('authInterceptor');

  }

  /**
   * You can intercept any request or response inside authInterceptor
   * or handle what should happend on 40x, 50x errors
   *
   */
  angular
    .module('boilerplate')
    .factory('authInterceptor', authInterceptor);

  authInterceptor.$inject = ['$rootScope', '$q', '$location'];
  function authInterceptor($rootScope, $q, $location) {

    return {
      // intercept every request
      request: function(config) {
        config.headers['Session'] = '.eJw9j0trg0AURv9KmbWLOHEldFE6iVR6rxjU4c5GUrXoPBLw0SSG_vdOsujywPcdOHdWf4_d1LN4HpcuYPXQsvjOXr5YzEhWliRcweUhisqhSy2IN44OIuJ5BEXqkNOFZGqVrpzSH5wKuFJhLWp0SuQRut1WJbRVwjoUO66SfY-6vJBDQ9J_15yTbiJYvVOjwcRvhO0z0fYgPK-Vy4qD575XsjKogdN60KTNmglzy4p2IFm-st-AHZt5-OnqY9Ocl9P8LAkDNnXTNJxPtelu_2XeaCCpDMh8Rt1c1ftmA2JvP2VqwMFMHDiu6Gsf1WX4sC9TNz6dzOMf3-9nSw.DJWT7A.idgUOfCn7uRtmXma6KJbQgQjTeA';
        return config;
      },
      response: function(res) {
        return res;
      },
      // Catch 404 errors
      responseError: function(response) {
        if (response.status === 404) {
          $location.path('/');
          return $q.reject(response);
        } else {
          return $q.reject(response);
        }
      }
    };
  }

  /**
   * Run block
   */
  angular
    .module('boilerplate')
    .run(run)
    .controller('AppController', AppController)
  ;

  run.$inject = ['$rootScope', '$location'];
  function run($rootScope, $location) {
  }

  AppController.$inject = ['$rootScope'];
  function AppController($rootScope) {
    $rootScope.headerVisibility = true;
  }

})();
