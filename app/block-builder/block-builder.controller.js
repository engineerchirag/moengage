;(function() {

  angular
    .module('boilerplate')
    .controller('BlockBuilderController', BlockBuilderController);

  BlockBuilderController.$inject = ['$scope', '$in', 'lodash', '$rootScope'];
  function BlockBuilderController($scope, $in, lodash, $rootScope) {
    $rootScope.headerVisibility = true;
    $('.toolbar .sortable-list .sortable-item').draggable({
      helper: 'clone'
    });

    $('.drawing-area .sortable-list').droppable({
      accept: '.sortable-item',
      drop: function(event, ui) {
        var droppedEleType = $(ui.draggable).clone().attr('data-type');
        var droppedEle = '<li class="selected-item" id="'+ droppedEleType +'"><div class="heading">'+ droppedEleType +'<span class="edit-option"><i class="fa fa-pencil" aria-hidden="true"></i></span></div></li>';
        $(this).append(droppedEle);
        $(".drawing-area .sortable-list .selected-item" ).resizable();
        $(".drawing-area .sortable-list").addClass("item");
        $(".item").removeClass("ui-draggable .sortable-item");
        $(".item").draggable({
          containment: 'parent',
          grid: [150,150]
        });
      }
    });
    $('.drawing-area .sortable-list').sortable();
    $(".drawing-area .sortable-list .selected-item").resizable();

    $('.drawing-area-footer .btn').click(function(){
      var code = $('.drawing-area .sortable-list').html();
      $('.output-box .code-box code').text(code);
    });

    $('body').on('click', '.drawing-area .sortable-list .selected-item .edit-option' ,(function(){
      var droppedEleType = $(this).parents('.selected-item').attr('id');
      $(this).parents('.selected-item').append(getDomFormElement(droppedEleType));
      $(this).hide();
    }));

    function getDomFormElement(eleType){
      var ele;
      if(eleType === 'paragraph'){
         ele = '<textarea class="paragraph"></textarea>';
      }else if(eleType === 'heading'){
        ele = '<input type="text"/>';
      }else if(eleType === 'link'){
        ele = '<input type="url"/>';
      }else if(eleType === 'image'){
        ele = '<input type="url"/>';
      }
      return ele;
    }

  }
})();
